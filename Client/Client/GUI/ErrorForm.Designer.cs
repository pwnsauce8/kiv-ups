﻿namespace Client.GUI
{
    partial class ErrorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.error_label = new System.Windows.Forms.Label();
            this.Close_client_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // error_label
            // 
            this.error_label.AutoSize = true;
            this.error_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.error_label.ForeColor = System.Drawing.Color.Crimson;
            this.error_label.Location = new System.Drawing.Point(229, 200);
            this.error_label.Name = "error_label";
            this.error_label.Size = new System.Drawing.Size(239, 24);
            this.error_label.TabIndex = 7;
            this.error_label.Text = "Server is not responding";
            this.error_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Close_client_btn
            // 
            this.Close_client_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(142)))), ((int)(((byte)(146)))));
            this.Close_client_btn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Close_client_btn.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Close_client_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Close_client_btn.Location = new System.Drawing.Point(270, 244);
            this.Close_client_btn.Name = "Close_client_btn";
            this.Close_client_btn.Size = new System.Drawing.Size(147, 28);
            this.Close_client_btn.TabIndex = 9;
            this.Close_client_btn.Text = "Start";
            this.Close_client_btn.UseVisualStyleBackColor = false;
            this.Close_client_btn.Click += new System.EventHandler(this.Close_client_btn_Click);
            // 
            // ErrorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 489);
            this.Controls.Add(this.error_label);
            this.Controls.Add(this.Close_client_btn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ErrorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ErrorForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label error_label;
        private System.Windows.Forms.Button Close_client_btn;
    }
}