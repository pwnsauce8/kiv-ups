﻿namespace Client
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.user_label = new System.Windows.Forms.ToolStripStatusLabel();
            this.connection_lable = new System.Windows.Forms.ToolStripStatusLabel();
            this.game_label = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.score_label = new System.Windows.Forms.Label();
            this.time_label = new System.Windows.Forms.Label();
            this.letter_label = new System.Windows.Forms.Label();
            this.lvl_label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Update_button = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.End_game_btn = new System.Windows.Forms.Button();
            this.Start_new_game_btn = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.userList = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.connection_panel = new System.Windows.Forms.Panel();
            this.game_list = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chatTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ip_text_box = new System.Windows.Forms.TextBox();
            this.user_text_box = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.port_text_box = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.Create_game_btn = new System.Windows.Forms.Button();
            this.Disconnect_bnt = new System.Windows.Forms.Button();
            this.Connect_btn = new System.Windows.Forms.Button();
            this.textInput = new System.Windows.Forms.TextBox();
            this.Send_msg_btn = new System.Windows.Forms.Button();
            this.score_table = new System.Windows.Forms.ListView();
            this.Nickname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Score = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.user_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.letter_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.state_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.town_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.river_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flower_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.animal_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.users_in_game_list = new System.Windows.Forms.ListBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.letter_label2 = new System.Windows.Forms.Label();
            this.animal_val = new System.Windows.Forms.TextBox();
            this.flower_val = new System.Windows.Forms.TextBox();
            this.river_val = new System.Windows.Forms.TextBox();
            this.town_val = new System.Windows.Forms.TextBox();
            this.state_val = new System.Windows.Forms.TextBox();
            this.game_timer = new System.Windows.Forms.Timer(this.components);
            this.ping_timer = new System.Windows.Forms.Timer(this.components);
            this.update_worker = new System.ComponentModel.BackgroundWorker();
            this.logger = new System.Windows.Forms.RichTextBox();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.connection_panel.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.user_label,
            this.connection_lable,
            this.game_label});
            this.statusStrip1.Location = new System.Drawing.Point(0, 495);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(715, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // user_label
            // 
            this.user_label.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.user_label.Name = "user_label";
            this.user_label.Size = new System.Drawing.Size(0, 17);
            // 
            // connection_lable
            // 
            this.connection_lable.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.connection_lable.ForeColor = System.Drawing.Color.Crimson;
            this.connection_lable.Name = "connection_lable";
            this.connection_lable.Size = new System.Drawing.Size(80, 17);
            this.connection_lable.Text = "Disconnected";
            // 
            // game_label
            // 
            this.game_label.Name = "game_label";
            this.game_label.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.tableLayoutPanel1);
            this.groupBox2.Location = new System.Drawing.Point(12, 14);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(377, 83);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Game Results";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.13836F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.86164F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel1.Controls.Add(this.score_label, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.time_label, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.letter_label, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lvl_label, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(371, 64);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // score_label
            // 
            this.score_label.AutoSize = true;
            this.score_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.score_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.score_label.Location = new System.Drawing.Point(268, 32);
            this.score_label.Name = "score_label";
            this.score_label.Size = new System.Drawing.Size(100, 32);
            this.score_label.TabIndex = 11;
            this.score_label.Text = "0";
            this.score_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // time_label
            // 
            this.time_label.AutoSize = true;
            this.time_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.time_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.time_label.ForeColor = System.Drawing.Color.Crimson;
            this.time_label.Location = new System.Drawing.Point(69, 32);
            this.time_label.Name = "time_label";
            this.time_label.Size = new System.Drawing.Size(85, 32);
            this.time_label.TabIndex = 6;
            this.time_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // letter_label
            // 
            this.letter_label.AutoSize = true;
            this.letter_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.letter_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.letter_label.Location = new System.Drawing.Point(3, 32);
            this.letter_label.Name = "letter_label";
            this.letter_label.Size = new System.Drawing.Size(60, 32);
            this.letter_label.TabIndex = 4;
            this.letter_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lvl_label
            // 
            this.lvl_label.AutoSize = true;
            this.lvl_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvl_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lvl_label.Location = new System.Drawing.Point(160, 32);
            this.lvl_label.Name = "lvl_label";
            this.lvl_label.Size = new System.Drawing.Size(102, 32);
            this.lvl_label.TabIndex = 5;
            this.lvl_label.Text = "0";
            this.lvl_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 32);
            this.label1.TabIndex = 7;
            this.label1.Text = "Letter";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(69, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 32);
            this.label2.TabIndex = 8;
            this.label2.Text = "Time";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(160, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 32);
            this.label3.TabIndex = 9;
            this.label3.Text = "Level";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(268, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 32);
            this.label4.TabIndex = 10;
            this.label4.Text = "Score";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Update_button);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.End_game_btn);
            this.panel1.Controls.Add(this.Start_new_game_btn);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(715, 103);
            this.panel1.TabIndex = 3;
            // 
            // Update_button
            // 
            this.Update_button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Update_button.Location = new System.Drawing.Point(556, 3);
            this.Update_button.Name = "Update_button";
            this.Update_button.Size = new System.Drawing.Size(107, 23);
            this.Update_button.TabIndex = 10;
            this.Update_button.Text = "Update";
            this.Update_button.Visible = false;
            this.Update_button.UseVisualStyleBackColor = true;
            this.Update_button.Click += new System.EventHandler(this.Update_button_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(669, 3);
            this.button1.Name = "button1";
            this.button1.Visible = false;
            this.button1.Size = new System.Drawing.Size(34, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Fix";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // End_game_btn
            // 
            this.End_game_btn.BackColor = System.Drawing.SystemColors.Control;
            this.End_game_btn.Enabled = false;
            this.End_game_btn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.End_game_btn.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.End_game_btn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.End_game_btn.Location = new System.Drawing.Point(556, 30);
            this.End_game_btn.Name = "End_game_btn";
            this.End_game_btn.Size = new System.Drawing.Size(147, 28);
            this.End_game_btn.TabIndex = 3;
            this.End_game_btn.Text = "Stop game";
            this.End_game_btn.UseVisualStyleBackColor = false;
            this.End_game_btn.Click += new System.EventHandler(this.End_game_btn_Click);
            // 
            // Start_new_game_btn
            // 
            this.Start_new_game_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(142)))), ((int)(((byte)(146)))));
            this.Start_new_game_btn.Enabled = false;
            this.Start_new_game_btn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Start_new_game_btn.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Start_new_game_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Start_new_game_btn.Location = new System.Drawing.Point(556, 63);
            this.Start_new_game_btn.Name = "Start_new_game_btn";
            this.Start_new_game_btn.Size = new System.Drawing.Size(147, 28);
            this.Start_new_game_btn.TabIndex = 2;
            this.Start_new_game_btn.Text = "Start";
            this.Start_new_game_btn.UseVisualStyleBackColor = false;
            this.Start_new_game_btn.Click += new System.EventHandler(this.Start_new_game_btn_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.userList);
            this.groupBox3.Location = new System.Drawing.Point(395, 14);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(141, 83);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "All Users";
            // 
            // userList
            // 
            this.userList.BackColor = System.Drawing.SystemColors.Control;
            this.userList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.userList.DisplayMember = "_user_name";
            this.userList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userList.FormattingEnabled = true;
            this.userList.Location = new System.Drawing.Point(3, 16);
            this.userList.Name = "userList";
            this.userList.Size = new System.Drawing.Size(135, 64);
            this.userList.TabIndex = 7;
            this.userList.ValueMember = "_user_name";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.connection_panel);
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.letter_label2);
            this.panel2.Controls.Add(this.animal_val);
            this.panel2.Controls.Add(this.flower_val);
            this.panel2.Controls.Add(this.river_val);
            this.panel2.Controls.Add(this.town_val);
            this.panel2.Controls.Add(this.state_val);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 103);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(715, 392);
            this.panel2.TabIndex = 3;
            // 
            // connection_panel
            // 
            this.connection_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.connection_panel.BackColor = System.Drawing.SystemColors.Control;
            this.connection_panel.Controls.Add(this.logger);
            this.connection_panel.Controls.Add(this.game_list);
            this.connection_panel.Controls.Add(this.chatTextBox);
            this.connection_panel.Controls.Add(this.tableLayoutPanel2);
            this.connection_panel.Controls.Add(this.panel5);
            this.connection_panel.Controls.Add(this.score_table);
            this.connection_panel.Location = new System.Drawing.Point(0, -3);
            this.connection_panel.Name = "connection_panel";
            this.connection_panel.Size = new System.Drawing.Size(715, 395);
            this.connection_panel.TabIndex = 2;
            // 
            // game_list
            // 
            this.game_list.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.game_list.Location = new System.Drawing.Point(12, 5);
            this.game_list.Name = "game_list";
            this.game_list.Size = new System.Drawing.Size(377, 166);
            this.game_list.TabIndex = 12;
            this.game_list.UseCompatibleStateImageBehavior = false;
            this.game_list.View = System.Windows.Forms.View.Details;
            this.game_list.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Game";
            this.columnHeader1.Width = 200;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Status";
            this.columnHeader2.Width = 164;
            // 
            // chatTextBox
            // 
            this.chatTextBox.Location = new System.Drawing.Point(12, 180);
            this.chatTextBox.Multiline = true;
            this.chatTextBox.Name = "chatTextBox";
            this.chatTextBox.Size = new System.Drawing.Size(377, 93);
            this.chatTextBox.TabIndex = 11;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.56902F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.43098F));
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.ip_text_box, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.user_text_box, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.port_text_box, 1, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(395, 180);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(308, 89);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 28);
            this.label6.TabIndex = 1;
            this.label6.Text = "Port:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 28);
            this.label7.TabIndex = 0;
            this.label7.Text = "IP:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ip_text_box
            // 
            this.ip_text_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ip_text_box.Location = new System.Drawing.Point(75, 3);
            this.ip_text_box.Name = "ip_text_box";
            this.ip_text_box.Size = new System.Drawing.Size(230, 20);
            this.ip_text_box.TabIndex = 2;
            this.ip_text_box.Text = "127.0.0.1";
            // 
            // user_text_box
            // 
            this.user_text_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.user_text_box.Enabled = false;
            this.user_text_box.Location = new System.Drawing.Point(75, 59);
            this.user_text_box.Name = "user_text_box";
            this.user_text_box.Size = new System.Drawing.Size(230, 20);
            this.user_text_box.TabIndex = 7;
            this.user_text_box.Text = "User1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(3, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 33);
            this.label8.TabIndex = 6;
            this.label8.Text = "User name:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // port_text_box
            // 
            this.port_text_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.port_text_box.Location = new System.Drawing.Point(75, 31);
            this.port_text_box.Name = "port_text_box";
            this.port_text_box.Size = new System.Drawing.Size(230, 20);
            this.port_text_box.TabIndex = 3;
            this.port_text_box.Text = "10007";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.Create_game_btn);
            this.panel5.Controls.Add(this.Disconnect_bnt);
            this.panel5.Controls.Add(this.Connect_btn);
            this.panel5.Controls.Add(this.textInput);
            this.panel5.Controls.Add(this.Send_msg_btn);
            this.panel5.Location = new System.Drawing.Point(0, 285);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(715, 43);
            this.panel5.TabIndex = 8;
            // 
            // Create_game_btn
            // 
            this.Create_game_btn.Enabled = false;
            this.Create_game_btn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Create_game_btn.Location = new System.Drawing.Point(406, 9);
            this.Create_game_btn.Name = "Create_game_btn";
            this.Create_game_btn.Size = new System.Drawing.Size(95, 23);
            this.Create_game_btn.TabIndex = 8;
            this.Create_game_btn.Text = "Create Game";
            this.Create_game_btn.UseVisualStyleBackColor = true;
            this.Create_game_btn.Click += new System.EventHandler(this.Create_game_btn_Click);
            // 
            // Disconnect_bnt
            // 
            this.Disconnect_bnt.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Disconnect_bnt.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Disconnect_bnt.Location = new System.Drawing.Point(608, 9);
            this.Disconnect_bnt.Name = "Disconnect_bnt";
            this.Disconnect_bnt.Size = new System.Drawing.Size(95, 23);
            this.Disconnect_bnt.TabIndex = 1;
            this.Disconnect_bnt.Text = "Disconnect";
            this.Disconnect_bnt.UseVisualStyleBackColor = true;
            this.Disconnect_bnt.Click += new System.EventHandler(this.Disconnect_bnt_Click);
            // 
            // Connect_btn
            // 
            this.Connect_btn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Connect_btn.Location = new System.Drawing.Point(507, 9);
            this.Connect_btn.Name = "Connect_btn";
            this.Connect_btn.Size = new System.Drawing.Size(95, 23);
            this.Connect_btn.TabIndex = 0;
            this.Connect_btn.Text = "Connect";
            this.Connect_btn.UseVisualStyleBackColor = true;
            this.Connect_btn.Click += new System.EventHandler(this.Connect_btn_Click);
            // 
            // textInput
            // 
            this.textInput.Location = new System.Drawing.Point(113, 10);
            this.textInput.Name = "textInput";
            this.textInput.Size = new System.Drawing.Size(276, 20);
            this.textInput.TabIndex = 7;
            // 
            // Send_msg_btn
            // 
            this.Send_msg_btn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Send_msg_btn.Location = new System.Drawing.Point(12, 8);
            this.Send_msg_btn.Name = "Send_msg_btn";
            this.Send_msg_btn.Size = new System.Drawing.Size(95, 23);
            this.Send_msg_btn.TabIndex = 6;
            this.Send_msg_btn.Text = "Send";
            this.Send_msg_btn.UseVisualStyleBackColor = true;
            this.Send_msg_btn.Click += new System.EventHandler(this.Send_msg_btn_Click);
            // 
            // score_table
            // 
            this.score_table.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Nickname,
            this.Score});
            this.score_table.Location = new System.Drawing.Point(395, 5);
            this.score_table.Name = "score_table";
            this.score_table.Size = new System.Drawing.Size(308, 166);
            this.score_table.TabIndex = 9;
            this.score_table.UseCompatibleStateImageBehavior = false;
            this.score_table.View = System.Windows.Forms.View.Details;
            // 
            // Nickname
            // 
            this.Nickname.Text = "Nickname";
            this.Nickname.Width = 187;
            // 
            // Score
            // 
            this.Score.Text = "Score";
            this.Score.Width = 117;
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.SkyBlue;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Menu;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedVertical;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.DarkCyan;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.user_column,
            this.letter_column,
            this.state_column,
            this.town_column,
            this.river_column,
            this.flower_column,
            this.animal_column});
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(0, 48);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.ScrollBar;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(574, 265);
            this.dataGridView1.TabIndex = 0;
            // 
            // user_column
            // 
            this.user_column.HeaderText = "User";
            this.user_column.Name = "user_column";
            this.user_column.ReadOnly = true;
            this.user_column.Width = 95;
            // 
            // letter_column
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Crimson;
            this.letter_column.DefaultCellStyle = dataGridViewCellStyle19;
            this.letter_column.HeaderText = "Letter";
            this.letter_column.Name = "letter_column";
            this.letter_column.ReadOnly = true;
            this.letter_column.Width = 50;
            // 
            // state_column
            // 
            this.state_column.HeaderText = "State";
            this.state_column.Name = "state_column";
            this.state_column.ReadOnly = true;
            this.state_column.Width = 95;
            // 
            // town_column
            // 
            this.town_column.HeaderText = "Town";
            this.town_column.Name = "town_column";
            this.town_column.ReadOnly = true;
            this.town_column.Width = 95;
            // 
            // river_column
            // 
            this.river_column.HeaderText = "River";
            this.river_column.Name = "river_column";
            this.river_column.ReadOnly = true;
            this.river_column.Width = 95;
            // 
            // flower_column
            // 
            this.flower_column.HeaderText = "Flower";
            this.flower_column.Name = "flower_column";
            this.flower_column.ReadOnly = true;
            this.flower_column.Width = 95;
            // 
            // animal_column
            // 
            this.animal_column.HeaderText = "Animal";
            this.animal_column.Name = "animal_column";
            this.animal_column.ReadOnly = true;
            this.animal_column.Width = 95;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.users_in_game_list);
            this.groupBox1.Location = new System.Drawing.Point(576, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(139, 313);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Users in game";
            // 
            // users_in_game_list
            // 
            this.users_in_game_list.BackColor = System.Drawing.SystemColors.Control;
            this.users_in_game_list.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.users_in_game_list.DisplayMember = "_user_name";
            this.users_in_game_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.users_in_game_list.FormattingEnabled = true;
            this.users_in_game_list.Location = new System.Drawing.Point(3, 16);
            this.users_in_game_list.Name = "users_in_game_list";
            this.users_in_game_list.Size = new System.Drawing.Size(133, 294);
            this.users_in_game_list.TabIndex = 8;
            this.users_in_game_list.ValueMember = "_user_name";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label13.Location = new System.Drawing.Point(482, 1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 18);
            this.label13.TabIndex = 20;
            this.label13.Text = "Animal";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label12.Location = new System.Drawing.Point(375, 1);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 18);
            this.label12.TabIndex = 19;
            this.label12.Text = "Flower";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label11.Location = new System.Drawing.Point(275, 1);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 18);
            this.label11.TabIndex = 18;
            this.label11.Text = "River";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label10.Location = new System.Drawing.Point(168, 1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 18);
            this.label10.TabIndex = 17;
            this.label10.Text = "Town";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label9.Location = new System.Drawing.Point(57, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 18);
            this.label9.TabIndex = 16;
            this.label9.Text = "State";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // letter_label2
            // 
            this.letter_label2.AutoSize = true;
            this.letter_label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.letter_label2.ForeColor = System.Drawing.Color.Crimson;
            this.letter_label2.Location = new System.Drawing.Point(12, 22);
            this.letter_label2.Name = "letter_label2";
            this.letter_label2.Size = new System.Drawing.Size(0, 18);
            this.letter_label2.TabIndex = 15;
            this.letter_label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // animal_val
            // 
            this.animal_val.Location = new System.Drawing.Point(460, 22);
            this.animal_val.Name = "animal_val";
            this.animal_val.Size = new System.Drawing.Size(100, 20);
            this.animal_val.TabIndex = 14;
            // 
            // flower_val
            // 
            this.flower_val.Location = new System.Drawing.Point(354, 22);
            this.flower_val.Name = "flower_val";
            this.flower_val.Size = new System.Drawing.Size(100, 20);
            this.flower_val.TabIndex = 13;
            // 
            // river_val
            // 
            this.river_val.Location = new System.Drawing.Point(248, 22);
            this.river_val.Name = "river_val";
            this.river_val.Size = new System.Drawing.Size(100, 20);
            this.river_val.TabIndex = 12;
            // 
            // town_val
            // 
            this.town_val.Location = new System.Drawing.Point(142, 22);
            this.town_val.Name = "town_val";
            this.town_val.Size = new System.Drawing.Size(100, 20);
            this.town_val.TabIndex = 11;
            // 
            // state_val
            // 
            this.state_val.Location = new System.Drawing.Point(36, 22);
            this.state_val.Name = "state_val";
            this.state_val.Size = new System.Drawing.Size(100, 20);
            this.state_val.TabIndex = 10;
            // 
            // game_timer
            // 
            this.game_timer.Interval = 1000;
            this.game_timer.Tick += new System.EventHandler(this.game_timer_Tick);
            // 
            // update_worker
            // 
            this.update_worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.update_worker_DoWork);
            // 
            // logger
            // 
            this.logger.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.logger.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.logger.Location = new System.Drawing.Point(0, 323);
            this.logger.Name = "logger";
            this.logger.ReadOnly = true;
            this.logger.Size = new System.Drawing.Size(715, 72);
            this.logger.TabIndex = 13;
            this.logger.Text = "";
            // 
            // GameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 517);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.Name = "GameForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GameForm_FormClosing);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.connection_panel.ResumeLayout(false);
            this.connection_panel.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lvl_label;
        private System.Windows.Forms.Label letter_label;
        private System.Windows.Forms.Label time_label;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Timer game_timer;
        private System.Windows.Forms.ToolStripStatusLabel connection_lable;
        private System.Windows.Forms.Button Start_new_game_btn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripStatusLabel user_label;
        private System.Windows.Forms.ListBox userList;
        private System.Windows.Forms.Timer ping_timer;
        private System.Windows.Forms.Panel connection_panel;
        private System.Windows.Forms.TextBox chatTextBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox ip_text_box;
        private System.Windows.Forms.TextBox user_text_box;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox port_text_box;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button Disconnect_bnt;
        private System.Windows.Forms.Button Connect_btn;
        private System.Windows.Forms.TextBox textInput;
        private System.Windows.Forms.Button Send_msg_btn;
        private System.Windows.Forms.Button End_game_btn;
        private System.Windows.Forms.Label score_label;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView score_table;
        private System.Windows.Forms.ColumnHeader Nickname;
        private System.Windows.Forms.ColumnHeader Score;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListView game_list;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ListBox users_in_game_list;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Create_game_btn;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label letter_label2;
        private System.Windows.Forms.TextBox animal_val;
        private System.Windows.Forms.TextBox flower_val;
        private System.Windows.Forms.TextBox river_val;
        private System.Windows.Forms.TextBox town_val;
        private System.Windows.Forms.TextBox state_val;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn user_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn letter_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn state_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn town_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn river_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn flower_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn animal_column;
        private System.Windows.Forms.ToolStripStatusLabel game_label;
        private System.Windows.Forms.Button Update_button;
        private System.ComponentModel.BackgroundWorker update_worker;
        private System.Windows.Forms.RichTextBox logger;
    }
}

