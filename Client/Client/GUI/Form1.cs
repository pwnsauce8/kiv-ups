﻿using Client.GUI;
using Client.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class GameForm : Form
    {
        public ClientSettings Client { get; set; }
        private User new_user;
        private static Random random = new Random();

        private const int _MAX_LEVEL = 7;
        private const int _MAX_TIME = 10;
        private int DURATION = _MAX_TIME;
        private string last_letter = null;
        private string selected_game;
        OperatingSystem os = Environment.OSVersion;

        private Game _game;

        public GameForm()
        {
            InitializeComponent();
            Words_enable(false);
            user_text_box.Text = RandomString(6);
            // Innit logger
            Logger.Instance.Innit_logger(logger);

            if (os.Platform == PlatformID.Unix)
            {
                button1.Visible = true;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public void _client_Received(ClientSettings cs, string received)
        {
            try
            {
                if (!received.Contains("<<<"))
                {
                    //Client.Close();
                    return;
                }
                string msg = received.Remove(0, 3);
                var split = msg.Split('!');

                // Create user if not exist
                if (new_user == null && split[1].Contains(":"))
                {
                    string user = split[1];
                    string[] id = user.Split(':');
                    new_user = new User(id[0], Int32.Parse(id[1]));
                }

                switch (split[0])
                {
                    case "USERS":
                        userList.Items.Clear();
                        for (int i = 2; i < split.Length; i++)
                        {
                            string[] id = split[i].Split(':');
                            if (id.Length != 2) continue;
                            userList.Items.Add(new User(id[0], Int32.Parse(id[1])));
                        }

                        logger.Text += "[INFO] User list updated" + "\r\n";
                        break;
                    case "RCHAT":
                        chatTextBox.Text += split[1] + "\r\n";
                        break;
                    case "DISCON":
                        ClearOldGame();
                        userList.Items.Clear();
                        chatTextBox.Text = string.Empty;
                        break;
                    case "GSTART":
                        // Enable Start button
                        Start_new_game_btn.Enabled = false;
                        // Change game status
                        ListViewItem item = game_list.FindItemWithText(selected_game);
                        if (item != null)
                        {
                            item.SubItems[1].Text = "1";
                        }
                        // Clear old game
                        this.Invoke(new Action(() =>
                        {
                            ClearOldGame();
                            StartTimer();
                        }));

                        Logger.Instance.Add_log("info", "_client_Received.GSTART", "Game #" + selected_game + " started");

                        break;
                    case "GURES":
                        Clear_all_items_in_list(score_table);
                        // Stop timer 
                        StopTimer();
                        // Clear old game
                        ClearOldGame();
                        // Add winnerss
                        this.Invoke(new Action(() =>
                        {
                            for (int i = 1; i < split.Length; i++)
                            {
                                string[] id = split[i].Split(':');
                                if (id.Length != 2) continue;
                                Add_to_score_table(id[0], id[1]);
                            }
                        }));

                        Logger.Instance.Add_log("info", "_client_Received.GURES", "Game results list updated");
                        break;
                    case "GRES":
                        try
                        {
                            this.Invoke(new Action(() =>
                            {
                                dataGridView1.Rows.Clear();
                            }));

                            for (int i = 1; i < split.Length; i++)
                            {
                                string[] words = split[i].Split(':');
                                if (words.Length != 7) continue;
                                this.Invoke(new Action(() =>
                                {
                                    dataGridView1.Rows.Add(words[0], last_letter, words[2], words[3], words[4], words[5], words[6]);
                                }));
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.Write("ERROR (_client_Received.GRES): " + ex);
                        }

                        break;
                    case "GLIST":
                        Clear_all_items_in_list(game_list);
                        for (int i = 2; i < split.Length; i++)
                        {
                            string[] id = split[i].Split(':');
                            if (id.Length != 2) continue;
                            Add_game_to_list(id[0], id[1]);
                        }

                        Logger.Instance.Add_log("info", "_client_Received.GLIST", "Game list updated");
                        break;
                    case "GUSERLIST":
                        users_in_game_list.Items.Clear();
                        for (int i = 2; i < split.Length; i++)
                        {
                            if (split[i].Contains("<<<")) { break; }
                            users_in_game_list.Items.Add(split[i]);
                        }
                        Logger.Instance.Add_log("info", "_client_Received.GUSERLIST", "Game users list updated");
                        break;
                    case "SERVEREND":
                        DialogResult dialogResult = MessageBox.Show("Server error", "Server is not responding", MessageBoxButtons.YesNo);
                        Logger.Instance.Add_log("error", "_client_Received.SERVEREND", "Server is not responding");
                        if (dialogResult == DialogResult.Yes)
                        {
                            Close();
                        }
                        break;
                    case "GLETTER":
                        string l = split[1];
                        new_letter = l[0].ToString();
                        break;
                    case "SERROR":
                        StopTimer();

                        this.Invoke(new Action(() =>
                        {
                            Logger.Instance.Add_log("ERROR", "_client_Received.SERROR", "Lost connection to server. Disconnecting..");
                            Disconnect();
                        }));

                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Add_log("ERROR", "_client_Received.GLIST", ex.ToString());
            }
        }

        #region CONNECTION
        private void Client_Disconnected(ClientSettings cs)
        {

        }

        private bool uncheck = true;
        private void Connect_btn_Click(object sender, EventArgs e)
        {
            if (uncheck)
            {
                string _username;
                string _ip = "";
                int _port;

                // Create client
                Client = new ClientSettings();
                Client.Received += _client_Received;
                Client.Disconnected += Client_Disconnected;

                // Check user name 
                if (user_text_box.Text.Length == 0)
                {
                    MessageBox.Show("Nick must be at least 1 character long!", "Invalid username",
                                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (user_text_box.Text.Contains('<') || user_text_box.Text.Contains('!') || user_text_box.Text.Contains(':'))
                {
                    MessageBox.Show("Nick can't contains '<', '!', ':' characters!", "Invalid username",
                                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                _username = user_text_box.Text;

                // Check IP adress
                string[] ip = ip_text_box.Text.Split('.');
                if (ip.Count() != 4)
                {
                    MessageBox.Show("Invalid IP format!", "Invalid IP",
                                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                _ip = ip_text_box.Text;

                // Check port
                if (!int.TryParse(port_text_box.Text, out _port))
                {
                    MessageBox.Show("Invalid port!", "Invalid port",
                                     MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Connect
                Client.Connected += Client_Connected;
                Client.Connect(_ip, _port);
            }
            else
            {
                user_text_box.Enabled = false;
                Connect_btn.Enabled = false;
                Create_game_btn.Enabled = true;
                user_label.Text = user_text_box.Text;
                Client.Send("CON", user_text_box.Text, "");
            }
        }

        private void Client_Connected(object sender, EventArgs e)
        {
            // Connection status
            connection_lable.Text = "Conencted";
            connection_lable.ForeColor = Color.Lime;

            // Login
            Connect_btn.Text = "Login";
            user_text_box.Enabled = true;
            ip_text_box.Enabled = false;
            port_text_box.Enabled = false;
            uncheck = false;

            // Logger
            Logger.Instance.Add_log("INFO", "Client_Connected", "Connected to server " + ip_text_box.Text + " on port " + port_text_box.Text);
            update_worker.RunWorkerAsync();
        }

        private void Disconnect_bnt_Click(object sender, EventArgs e)
        {
            try
            {
                Client.Send("DISCON", new_user._user_id.ToString(), "");

            }
            catch (Exception ex)
            {
                Logger.Instance.Add_log("ERROR", "Send_msg_btn_Click", ex.ToString());
                Client = null;
            }
            Disconnect();
            ClearOldGame();
            userList.Items.Clear();
            chatTextBox.Text = string.Empty;
            Clear_all_items_in_list(score_table);
            Clear_all_items_in_list(game_list);
        }

        private void Disconnect()
        {
            ClearOldGame();
            userList.Items.Clear();
            Connect_btn.Enabled = true;
            Connect_btn.Text = "Connect";
            user_text_box.Enabled = false;
            ip_text_box.Enabled = true;
            port_text_box.Enabled = true;
            uncheck = true;
        }

        private void Send_msg_btn_Click(object sender, EventArgs e)
        {
            if (textInput.Text != string.Empty)
            {
                try
                {
                    Client.Send("Message", new_user._user_id.ToString(), textInput.Text);
                }
                catch (Exception ex) { Logger.Instance.Add_log("ERROR", "Send_msg_btn_Click", ex.ToString()); }

                textInput.Text = string.Empty;
            }
        }

        #endregion

        #region GAME

        private void Start_new_game_btn_Click(object sender, EventArgs e)
        {
            Client.Send("GSTART", new_user._user_id.ToString(), _game._ID.ToString());
        }

        private void End_game_btn_Click(object sender, EventArgs e)
        {
            StopTimer();
            Words_enable(false);
            Connection_panel_open(true);
            Client.Send("GDISCON", new_user._user_id.ToString(), _game._ID.ToString());
            ClearOldGame();
            update_worker.RunWorkerAsync();
        }

        private void StartTimer()
        {
            game_timer.Enabled = true;
            game_timer.Start();
        }

        private void StopTimer()
        {
            game_timer.Enabled = false;
            game_timer.Stop();
        }


        private void ClearOldGame()
        {
            DURATION = _MAX_TIME;
            letter_label.Text = "";
            letter_label2.Text = "";
            time_label.Text = "";
            lvl_label.Text = "0";
            score_label.Text = "0";
            dataGridView1.Rows.Clear();
        }

        private void Add_to_score_table(string nickname, string score)
        {
            ListViewItem lvi = new ListViewItem(nickname);
            lvi.SubItems.Add(score);
            score_table.Items.Add(lvi);
        }

        private void Add_game_to_list(string game_id, string status)
        {
            ListViewItem lvi = new ListViewItem(game_id);
            lvi.SubItems.Add(status);
            game_list.Items.Add(lvi);
        }
        #endregion


        private void button1_Click(object sender, EventArgs e)
        {
            Connection_panel_open(false);
            Connection_panel_open(true);
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var senderList = (ListView)sender;
            var clickedItem = senderList.HitTest(e.Location).Item;
            //if (clickedItem != null && clickedItem.SubItems[1].Text == "0")
            if (clickedItem.SubItems[1].Text == "1")
            {
                StartTimer();
            }
            if (clickedItem != null)
            {
                // Connect to game
                Client.Send("GCON", new_user._user_id.ToString(), clickedItem.Text);
                game_label.Text = clickedItem.Text;
                // Create Game
                _game = new Game(Int32.Parse(clickedItem.Text));
                _game.StartGame(letter_label, lvl_label, dataGridView1);
                selected_game = clickedItem.Text;
                Connection_panel_open(false);
            }
            else
            {
                MessageBox.Show("Sorry, you cant");
            }
        }

        private void Connection_panel_open(bool open)
        {
            connection_panel.Visible = open;
            End_game_btn.Enabled = !open;
            Start_new_game_btn.Enabled = !open;
        }


        private void Create_game_btn_Click(object sender, EventArgs e)
        {
            // Create game
            try
            {
                Client.Send("GCREAT", new_user._user_id.ToString(), user_text_box.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid server");
                Close();
            }

        }

        private void Clear_all_items_in_list(ListView list)
        {
            list.Items.Clear();
            list.Refresh();
        }

        string new_letter = "";
        private void game_timer_Tick(object sender, EventArgs e)
        {
            // End game
            if (Int32.Parse(lvl_label.Text) == _MAX_LEVEL)
            {
                Words_enable(false);
                // Send results
                Client.Send("GURES", new_user._user_id.ToString(), new_user._score.ToString());
                this.Invoke(new Action(() =>
                {
                    MessageBox.Show("Game end");
                }));
                return;
            }

            DURATION--;
            time_label.Text = DURATION.ToString();

            // Get new letter
            if (DURATION == 4)
            {
                // new_letter = _game.GetLetter().ToString().ToUpper();
                Client.Send("GLETTER", new_user._user_id.ToString(), user_text_box.Text);
            }

            // Get new level
            if (DURATION == -1)
            {
                try
                {
                    Words_enable(true);
                    state_val.Select();
                    // Level up
                    int new_lvl = Int32.Parse(lvl_label.Text) + 1;
                    lvl_label.Text = new_lvl.ToString();
                    // Update timer
                    time_label.Text = _MAX_TIME.ToString();
                    // Save last letter
                    last_letter = letter_label.Text;
                    // Set new letter
                    letter_label.Text = new_letter;
                    letter_label2.Text = new_letter;
                    // Create new level
                    string result = Get_words();
                    Client.Send("GRES", new_user._user_id.ToString(), Get_words());
                    Clear_words();

                    // Update score
                    if (Int32.Parse(lvl_label.Text) > 1)
                    {
                        int score = _game.Check_Words(last_letter, result);
                        int res = Int32.Parse(score_label.Text) + score;
                        score_label.Text = res.ToString();
                        new_user.Set_score(Int32.Parse(score_label.Text));
                    }

                    DURATION = _MAX_TIME;
                }
                catch (Exception ex)
                {
                    Logger.Instance.Add_log("ERROR", "game_timer_Tick", ex.ToString());
                }

            }
        }

        private string Get_words()
        {
            string result = "";

            result += new_user._user_name;
            result += ":";
            result += state_val.Text.ToLower();
            result += ":";
            result += town_val.Text.ToLower();
            result += ":";
            result += river_val.Text.ToLower();
            result += ":";
            result += flower_val.Text.ToLower();
            result += ":";
            result += animal_val.Text.ToLower();

            return result;
        }

        private void Clear_words()
        {
            state_val.Text = string.Empty;
            town_val.Text = string.Empty;
            river_val.Text = string.Empty;
            flower_val.Text = string.Empty;
            animal_val.Text = string.Empty;
        }

        private void Words_enable(bool enable)
        {
            state_val.Enabled = enable;
            town_val.Enabled = enable;
            river_val.Enabled = enable;
            flower_val.Enabled = enable;
            animal_val.Enabled = enable;
        }

        private void GameForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Client.Send("DISCON", new_user._user_id.ToString(), "");
            }
            catch (Exception ex) { Logger.Instance.Add_log("ERROR", "GameForm_FormClosing", ex.ToString()); }
        }

        private void Update_button_Click(object sender, EventArgs e)
        {

            if (update_worker.IsBusy != true)
            {
                update_worker.RunWorkerAsync();
            }
        }

        private void update_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Thread.Sleep(2000);
                // Update users list
                Client.Send("GLIST", new_user._user_id.ToString(), "");
            }
            catch (Exception ex)
            {
                Logger.Instance.Add_log("ERROR", "update_worker_DoWork", ex.ToString());
            }
        }
    }
}
