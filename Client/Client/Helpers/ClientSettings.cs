﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace Client
{
    public class ClientSettings
    {
        readonly Socket _s;
        public delegate void ReceivedEventHandler(ClientSettings cs, string received);
        public event ReceivedEventHandler Received = delegate { };
        public event EventHandler Connected = delegate { };
        public delegate void DisconnectedEventHandler(ClientSettings cs);
        public event DisconnectedEventHandler Disconnected = delegate { };
        public bool _connected;
        public bool _ping;

        public ClientSettings()
        {
            _s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public bool Connect(string ip, int port)
        {
            try
            {
                var ep = new IPEndPoint(IPAddress.Parse(ip), port);
                _s.BeginConnect(ep, ConnectCallback, _s);
                return true;
            }
            catch {
                return false;
            }
        }

        public void Close()
        {
            _s.Dispose();
            _s.Close();
        }

        void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                _s.EndConnect(ar);
                _connected = true;
                Connected(this, EventArgs.Empty);
                var buffer = new byte[_s.ReceiveBufferSize];
                _s.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReadCallback, buffer);
            }
            catch {
                Disconnected(this);
                Server_error();
            }
        }

        private void ReadCallback(IAsyncResult ar)
        {
            try
            {
                var buffer = (byte[])ar.AsyncState;
                var rec = _s.EndReceive(ar);
                if (rec != 0)
                {
                    var data = Encoding.ASCII.GetString(buffer, 0, rec);
                    Received(this, data);
                }
                else
                {
                    Disconnected(this);
                    Close();
                    return;
                }
                _s.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReadCallback, buffer);
            }
            catch
            {
                Server_error();
                Close();
            }
        }

        private void Server_error()
        {
            MessageBox.Show("Server is not responding", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void Send(string command, string username, string data)
        {
            string result = "<<<" + command.ToUpper() + "!" + username + "!" + data + "\0";
            try
            {
                var buffer = Encoding.ASCII.GetBytes(result);
                _s.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, SendCallback, buffer);
            }
            catch {
                Server_error();
            }
        }

        void SendCallback(IAsyncResult ar)
        {
            int i = _s.EndSend(ar);
            if (i > 0)
            {
                _ping = true;
            }
            else
            {
                _ping = false;
            }
        }
    }
}