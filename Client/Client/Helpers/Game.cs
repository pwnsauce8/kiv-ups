﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client.Helpers
{
    class Game
    {
        private static Random _random = new Random();
        private const int _MAX_GAME_LVL = 10;
        private Label _letter;
        private Label _level;
        private DataGridView _dgv;
        public int _ID;
        private int last_ind = 0;

        public Game(int id)
        {
            _ID = id;
        }


        internal char GetLetter()
        {
            int num = _random.Next(0, 26);
            char let = (char)('a' + num);
            return let;
        }

        internal void StartGame(Label letter_label, Label lvl_label, DataGridView dataGridView1)
        {
            // Create constants
            _letter = letter_label;
            _level = lvl_label;
            _dgv = dataGridView1;
        }

        internal int New_level(string letter, int level)
        {
            this._dgv.Rows.Add(letter, "", "", "", "", "");
            _dgv.Rows[_dgv.Rows.Count - 2].ReadOnly = false;              // current row
            if (level != 1)
            {
                _dgv.Rows[_dgv.Rows.Count - 3].ReadOnly = true;           // last row
                _dgv.Rows[_dgv.Rows.Count - 1].ReadOnly = true;           // next row
                return 1;
            }
            return 0;
        }

        internal List<string> SendRowData(int i)
        {
            last_ind = i;
            List<string> result = new List<string>();
            DataGridViewRow row = _dgv.Rows[i];

            for (int j = 1; j < row.Cells.Count; j++)
            {
                result.Add(row.Cells[j].Value.ToString().ToLower());
            }

            return result;
        }

        internal void SendLast()
        {
            SendRowData(last_ind + 1);
        }

        /// <summary>
        /// word1!word2!word3!word4!word5!word6
        /// </summary>
        /// <param name="letter"></param>
        /// <param name="str"></param>
        internal int Check_Words(string letter, string words)
        {
            string[] words_list = words.Split(':');
            int result = 0;
            letter = letter.ToLower();

            foreach (string word in words_list)
            {
                if (word == "") continue;
                if (word[0] == letter[0])
                    result++;
            }

            return result * 10;
        }
    }
}
