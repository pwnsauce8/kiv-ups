﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;

namespace Client.Helpers
{
    public class Logger
    {
        /// <summary>
        /// Constant interval for running each thread
        /// </summary>
        /// <summary>
        /// Instance of this class
        /// </summary>
        private static Logger _instance;
        private static RichTextBox _logger;
        /// <summary>
        /// Property for getting singleton instance
        /// </summary>
        public static Logger Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Logger();
                }
                return _instance;
            }
        }

        OperatingSystem os = Environment.OSVersion;
        private Logger() { }

        /// <summary>
        /// Innit logger
        /// </summary>
        /// <param name="user">User.</param>
        public void Innit_logger(RichTextBox logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Update User XML
        /// </summary>
        /// <param name="user">User.</param>
        public void Add_log(string level, string method, string txt)
        {
            if (os.Platform != PlatformID.Unix)
            {
                level = level.ToUpper();
                string text = "[" + level + "] " + "(" + method + ") " + txt + "\n";
                _logger.AppendText(text);
            }

        }
    }
}
