﻿using System;
namespace Client.Helpers
{
    public class User
    {
        public string _user_name { get; }
        public int _score { get; set; }
        public int _user_id { get; set; }
        /// <summary>
        /// 0 - waiting
        /// 1 - in game
        /// </summary>
        /// <value>The status.</value>
        public int _status { get; set; }
        public bool _finished { get; set; }

        public User(string user_name, int user_id)
        {
            _user_name = user_name;
            _score = 0;
            _status = 0;
            _finished = false;
            _user_id = user_id;
        }

        public void Set_status (int status) {
            _status = status;
        }

        public void Set_score(int score)
        {
            _score = score;
        }

        public void Set_finish(bool finish)
        {
            _finished = finish;
        }
    }
}
