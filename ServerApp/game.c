#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>

#include "player.h"
#include "game.h"
#include "server.h"
#include "logger.h"


const int _TIMER_MAX = 10;
const int _LEVEL_MAX = 8;
const int _ALPHA_SIZE = 27;
const char _ALPHABETH[] = { 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' };
int _STOP = 0;

games_t *games = NULL;                                // Root seznamu her
pthread_mutex_t m_games = PTHREAD_MUTEX_INITIALIZER;  // Mutex pro pristup k seznamu

void set_game_status (int status) {
    pthread_mutex_lock(&m_games);
    _STOP = status;
    pthread_mutex_unlock(&m_games);
}

/*
 * Find game in list by ID
 */
games_t *find_game(int ID) {
    pthread_mutex_lock(&m_games);
    if(games != NULL)
    {
        games_t *ptr = games;
        do
        {
            if(ptr->ID == ID)
            {
                pthread_mutex_unlock(&m_games);
                return ptr;
            }
            ptr = ptr->next;
        }while(ptr != NULL);
    }
    pthread_mutex_unlock(&m_games);
    return NULL;
}

/*
 * Create new game
 */
int create_game(players_t *player) {
    if (player == NULL) {
        return 0;
    }

    games_t *game = malloc(sizeof(games_t));
    game->ID = getID();
    game->name = strdup("Game #");
    sprintf(game->name, "%s %d", game->name, game->ID);

    game->next = NULL;
    game->players_count = 0;
    game->in_game = 0;

    // Vytvorit instanci hracu
    int i;
    for(i = 0; i < MAX_PLAYERS_COUNT; i++)
    {
        (game->players)[i] = NULL;
    }

    char *log = malloc(sizeof(char) * 50);
    sprintf(log, "New game created: %s", game->name);
    add_log('s', log);
    free(log);

    add_game(game);
    return game->ID;
}

/*
 * Add existing game to list
 */
void add_game(games_t *game) {
    if(!game)
        return;

    pthread_mutex_lock(&m_games);
    if(games == NULL)
    {
        games = game;
    }
    else
    {
        games_t *gm = games;

        while(gm->next != NULL)
        {
            gm = gm->next;
        }

        gm->next = game;

    }
    pthread_mutex_unlock(&m_games);
}

/*
 * Delete game from list
 */
void delete_game(games_t *game)  {
    if(!game)
        return;

    char *txt = (char*)malloc(1024);
    int id = game->ID;
    games_t *ptr = games;
    games_t *previous = NULL;

    char *log = malloc(100 * sizeof(char));
    sprintf(log, "Game ended: %s", game->name);

    pthread_mutex_lock(&m_games);
    do
    {
        if(ptr->ID == id)
        {
            if(previous == NULL)
            {
                if(ptr->next == NULL)
                {
                    free_game(ptr);
                    games = NULL;
                }
                else
                {
                    games = ptr->next;
                    free_game(ptr);
                }
            }
            else
            {
                previous->next = ptr->next;
                free_game(ptr);
                ptr = NULL;
            }
            break;
        }

        previous = ptr;
        ptr = ptr->next;

    } while(ptr != NULL);
    pthread_mutex_unlock(&m_games);

    broadcastData("UPDATE", get_games_list());

    add_log('s', log);
    free(txt);
    free(log);
}

/*
 * Free one game
 */
void free_game(games_t *game) {
    if(!game)
        return;

    free(game->name);
    free(game);
}

/*
 * Send message to all players in game
 */
void send_msg_to_all_players(games_t *game, char *msg) {
    if(!game || !msg)
        return;

    int i;
    for(i = 0; i < MAX_PLAYERS_COUNT; i++)
    {
        if((game->players)[i])
        {
            send((game->players)[i]->socket , msg , strlen(msg) , 0 );
            add_log('c', msg);
        }
    }
}

/*
 * Free whole game list
 */
void free_game_list() {
    while(games)
    {
        delete_game(games);
    }
}

/*
 * Get string in format "!Username:user_id!.." with all users
 */
char *get_games_list() {
    char *result = (char*)malloc(1024);
    char append[25];
    games_t *ptr = games;
    strcpy(result, "!");

    if(games == NULL)
        return "";

    for(; ptr != NULL; ptr = ptr->next) {
        sprintf(append, "%d", ptr->ID);
        strcat(result, append);
        strcat(result, ":");
        sprintf(append, "%d", ptr->in_game);
        strcat(result, append);
        strcat(result, "!");
    }
    strcat(result, "\0");
    return result;
}

/*
 * Get string in format "!Username!.." with all users in ONE game
 */
char *get_game_players_list(games_t *game) {
    char *result = (char*)malloc(1024);
    int i;

    strcpy(result, "!");

    if(games == NULL)
        return NULL;

    for(i = 0; i < MAX_PLAYERS_COUNT; i++)
    {
        if((game->players)[i])
        {
            strcat(result, (game->players)[i]->name);
            strcat(result, "!");
        }
    }

    strcat(result, "\0");
    return result;
}

/*
 * Print game list
 */
void print_game_list () {
    games_t *ptr = games;
    printf("+=================== [GAMES LIST] =======================+\n");
    if(games != NULL)
    {
        do
        {
            printf("\t * Name %s\n", ptr->name);
            ptr = ptr->next;
        }while(ptr != NULL);
    }
    else printf("Empty game list\n");
    printf("=========================================================\n");
}

/*
 * Get randome range
 */
int randomRange(int min, int max)
{
    return ( rand() % ( max - min + 1 ) ) + min;
}

/*
 * Create random letter
 */
char get_letter()
{
    char letter;
    srand( (unsigned int)time ( NULL ) );

    letter = _ALPHABETH[randomRange(0, _ALPHA_SIZE - 1)];
    return letter;
}

/*
 * Create string with users and their scores
 */
char *get_users_score (games_t *game) {
    char *result = (char*)malloc(1024);
    int i;

    strcpy(result, "!");

    if(games == NULL)
        return NULL;

    for(i = 0; i < MAX_PLAYERS_COUNT; i++)
    {
        if((game->players)[i])
        {
            strcat(result, (game->players)[i]->name);
            strcat(result, ":");
            strcat(result, (game->players)[i]->score);
            strcat(result, "!");
        }
    }

    strcat(result, "\0");
    return result;
}

/*
 * Create string with users and their results
 */
char *get_users_results (games_t *game) {
    char *result = (char*)malloc(1024);
    int i;

    strcpy(result, "!");

    if(games == NULL)
        return NULL;

    for(i = 0; i < MAX_PLAYERS_COUNT; i++)
    {
        if((game->players)[i])
        {
            strcat(result, (game->players)[i]->name);
            strcat(result, ":");
            strcat(result, (game->players)[i]->last_result);
            strcat(result, "!");
        }
    }

    strcat(result, "\0");
    return result;
}











