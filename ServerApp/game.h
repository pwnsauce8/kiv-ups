#ifndef SERVER_GAME_H
#define SERVER_GAME_H

#include "structures.h"
#define MAX_PLAYERS_COUNT 10

games_t *find_game(int ID);                             // Najde hru podle ID
int create_game(players_t *player);                    // Vytvori hru
void add_game(games_t *hra);                            // Prida hru do seznamu
void delete_game(games_t *hra);                         // Odstrani hru ze seznamu
void free_game(games_t *hra);                           // Uvolni pamet hry
void send_msg_to_all_players(games_t *hra, char *msg);  // Rozeslat zpravu vsem hracum
void free_game_list();                                  // Uvolnit pamet celeho seznamu her
char *get_games_list();                                 // Vypsat seznam her
void print_game_list ();
char *get_game_players_list(games_t *game);
char *get_users_results (games_t *game);

void* game_timer(void *arg);
int randomRange(int min, int max);
char get_letter();
void set_game_status (int status);
char *find_winner (games_t *game);
char *get_users_score (games_t *game);

#endif //SERVER_GAME_H
