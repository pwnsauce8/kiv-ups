#include <time.h>
#include <stdio.h>
#include <memory.h>
#include <malloc.h>
#include "logger.h"

FILE *logger;
time_t start_time, end_time;
char *LOGGER_FILE = "/home/janelle/Projects/kiv-ups/ServerApp/Logs/logs.log";

/*
 * Logger innit
 */
void logger_init (int port) {
    char *t = get_current_time();

    start_time = time(NULL);
    logger = fopen(LOGGER_FILE, "w");
    if (logger == NULL) {
        printf("Error: Cant open logger file");
        return;
    }
    fclose(logger);
    logger = fopen(LOGGER_FILE, "a+");
    printf("%s\tServer will listen on port: %d\n", t, port);
    fprintf(logger,"%s\tServer will listen on port: %d\n", t, port);
    fprintf(logger,"\n");
    fclose(logger);
}

/*
 * Add log
 */
void add_log(char server, char *message) {
    char *time = get_current_time();

    printf("%s\t%s\n", time, message);
    // Append to logger file new log
    logger = fopen(LOGGER_FILE, "a+");
    if (logger == NULL) {
        printf("Error: Cant open logger file");
        return;
    }
    fprintf(logger, "%s\t%s\n", time, message);
    fprintf(logger,"\n");
    fclose(logger);
}

/*
 * End logger
 */
int logger_end () {
    int total_time;

    logger = fopen("server.log", "a+");

    end_time = time(NULL);
    total_time = (int) difftime(end_time, start_time);

    fprintf(logger,"%s\tServer ends. Total time: %d s.\n", get_current_time(), total_time);
    fprintf(logger,"\n");

    return total_time;
}

/*
 * Get current time
 */
char *get_current_time () {
    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    return asctime (timeinfo);
}