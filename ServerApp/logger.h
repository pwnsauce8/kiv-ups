#ifndef SERVER_LOGGER_H
#define SERVER_LOGGER_H


/* FUNCTIONS */
void logger_init (int port);
void add_log(char server, char *message);
int logger_end ();
char *get_current_time ();


#endif //SERVER_LOGGER_H
