#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "player.h"
#include "game.h"
#include "server.h"
#include "logger.h"

players_t *players = NULL; // Root seznamu hracu
pthread_mutex_t m_players = PTHREAD_MUTEX_INITIALIZER; // Mutex pro pristup k seznamu

/*
 * Find player in list by ID
 */
players_t *find_player(int ID) {

    if(!ID) {
        return NULL;
    }

    pthread_mutex_lock(&m_players);

    // Projit skrz cely seznam hracu
    if(players != NULL)
    {
        players_t *ptr = players;
        while(ptr != NULL)
        {
            // Porovnat ID
            if(ptr->ID ==  ID)
            {
                pthread_mutex_unlock(&m_players);
                return ptr;
            }

            ptr = ptr->next;

        }
    }
    pthread_mutex_unlock(&m_players);

    return NULL;
}

/*
 * Add player to list
 */
void add_player_to_list(players_t *player) {
    if(!player) {
        return;
    }

    pthread_mutex_lock(&m_players);

    // Jestli root
    if(players == NULL)
    {
        players = player;
    }
    else
    {
        players_t *ptr = players;

        while(ptr->next != NULL)
        {
            ptr = ptr->next;
        }

        ptr->next = player;
    }
    pthread_mutex_unlock(&m_players);
}

/*
 * Create new player
 */
players_t *create_player(int socket, char *name) {
    players_t *player = malloc(sizeof(players_t));
    player->name = strdup(name);

    player->socket = socket;
    player->score = (char*)malloc(1024);
    player->last_result = (char*)malloc(1024);
    player->ID = getID();
    player->next = NULL;
    player->game = NULL;

    return player;
}

/*
 * Get players list
 */
char *get_players_list() {
    players_t *ptr = players;
    char *result = (char*)malloc(1024);
    char append[25];

    strcpy(result, "!");

    if(players == NULL)
        return result;

    for(; ptr != NULL; ptr = ptr->next) {
        strcat(result, ptr->name);
        strcat(result, ":");
        sprintf(append, "%d", ptr->ID);
        strcat(result, append);
        strcat(result, "!");
    }
    strcat(result, "\0");
    return result;
}

/*
 * Delete player from list
 */
void delete_player_from_list(players_t *player) {
    if(!player)
        return;

    int id = player->ID;

    char *log = malloc(100 * sizeof(char));
    sprintf(log, "User #%d disconnected", player->ID);

    pthread_mutex_lock(&m_players);

    players_t *ptr = players;
    players_t *last = NULL;

    do
    {
        if(ptr->ID == id)
        {
            if(!last)
            {
                if(ptr->next == NULL)
                {
                    free_player(ptr);
                    players = NULL;
                }
                else
                {
                    players = ptr->next;
                    free_player(ptr);
                    ptr = NULL;
                }
            }
            else
            {
                last->next = ptr->next;
                free_player(ptr);
                ptr = NULL;
            }
            break;

        }

        last = ptr;
        ptr = ptr->next;

    } while(ptr != NULL);

    pthread_mutex_unlock(&m_players);

    broadcastData("USERS", get_players_list());
    add_log('s', log);
    free(log);
}

/*
 * Free one player
 */
void free_player(players_t *player) {
    free(player->name);
    free(player->score);
    free(player->last_result);
    free(player);
}

/*
 * Free whole players list
 */
void free_players_list()
{
    while(players)
    {
        delete_player_from_list(players);
    }
}

/*
 * Print all player on server
 */
void print_players_list() {
    players_t *ptr = players;
    printf("+=================== [PLAYERS LIST] =====================+\n");
    if(players != NULL)
    {
        do
        {
            printf("\t * Player #%d\n", ptr -> ID);
            ptr = ptr -> next;
        }while(ptr != NULL);
    }
    else printf("Players list is empty\n");

    printf("+========================================================+\n");
}
