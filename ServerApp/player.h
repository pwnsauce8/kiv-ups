#ifndef SERVERAPP_PLAYER_H
#define SERVERAPP_PLAYER_H

#include "structures.h"

players_t *find_player(int ID);                         // Najde hrace v seznamu
void add_player_to_list(players_t *hrac);               // Prida hrace do seznamu
players_t *create_player(int socket, char *name);       // Vytvori hrace
char *get_players_list();                               // Vypise seznam vsech hracu
void print_players_list();

// Free functions
void delete_player_from_list(players_t *player);          // Odstrani hrace z listu
void free_player(players_t *player);                      // Uvolni pamet jednoho hrace
void free_players_list();                                 // Uvolni cely list


#endif //SERVERAPP_PLAYER_H
