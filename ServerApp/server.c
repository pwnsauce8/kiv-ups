#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>

#include "server.h"
#include "logger.h"
#include "player.h"
#include "game.h"
#include "structures.h"

extern pthread_mutex_t m_players;
extern pthread_mutex_t m_games;
extern players_t *players;
extern games_t *games;
pthread_t thread_id;

time_t start, now;

// Statistics
long send_msg_count = 0;
long send_msg_bytes = 0;
long received_bytes = 0;
long received_msg = 0;
long error_msg = 0;

/*
 * Main function
 */
int main(int argc, char *argv[]) {
    int port;

    // Innit start time
    time(&start);
    // Inicializace randomu
    srand(time(NULL));

    // Kontrola vstupnich parametru
    if (argc < 2) {
        // Возможно добавить проверку порта
        using();
        return EXIT_FAILURE;
    }

    port = atoi(argv[1]);

    // Port control
    if(port > 1025 && port < 65536) {
        server_start(argv[1]);
    } else {
        port = 10001;
        server_start(integer_to_string(port));
    }

    // Inicializace loggeru
    logger_init(port);

    // Inicializace vlakna
    thread_id = 0;

    pthread_create(&thread_id, NULL, create_tcp_connection, (void *)&port);

    char cmd[1024];
    while(scanf("%c", cmd) != -1)
    {
        switch(toupper(cmd[0])) {

            case 'Q'  :
                // End server
                goto END;
            case 'G'  :
                // Print all games
                print_game_list();
                break;
            case 'P'  :
                // Print all players
                print_players_list();
                break;
            case 'I'  :
                // Print server information
                get_server_info();
                break;
        }
    }

    END: broadcastData("SERVEREND", "");

    // Free memory
    free_players_list();
    free_game_list();

    add_log('s', "Server end");
    get_server_info();

    // End logger
    logger_end();
    // End main thread
    pthread_cancel(thread_id);

    return EXIT_SUCCESS;
}

/*
 * Check if message is valid and create commands
 */
void check_valid_message (char* tokens[15], char command[64]) {
    char *cmd;
    int i = 0;

    if (command[0] == '<' && command[1] == '<' && command[2] == '<') {
        // Remove "<<<"
        cmd = strtok(command, "<<<");

        // Commands
        tokens[0] = strtok(cmd, "!");

        while (tokens[i] != NULL) {
            i++;
            tokens[i] = strtok(NULL, "!");
        }
    } else {
        error_msg++;
    }
}

/*
 * Processes message and call function in message is valid
 */
void parse_command(char *command[15])
{
    char *txt = (char*)malloc(1024);
    int id;

    players_t *player = NULL;
    games_t *game = NULL;

    if (command == NULL) {
        return;
    }

    // Find user if exist
    if (command[1] != NULL) {
        id = atoi(command[1]);
        player = find_player(id);

        if(!player)
        {
            invalid_command(command[0]);
            return;
        }
        game = player->game;
    } else {
        invalid_command(command[0]);
        free(txt);
        return;
    }

    // Other commands
    if  (strcmp(command[0], "MESSAGE") == 0) { /* <====================================== MESSAGE TO CHAT */
        // Check message
        if  (command[2] == NULL && command[3] == NULL) {
            invalid_command(command[0]);
            free(txt);
            return;
        }
        sprintf(txt, "%s says: %s", player->name, command[2]);
        broadcastData("RCHAT", txt);

    } else if (strcmp(command[0], "DISCON") == 0) { /* <================================= DISCONNECT */
        delete_player_from_list(player);
    } else if (strcmp(command[0], "GSTART") == 0) { /* <==================================== GAME START */
        // Check message
        if  (command[2] == NULL && command[3] == NULL) {
            invalid_command(command[0]);
            free(txt);
            return;
        }

        // Logger
        sprintf(txt, "New game started");
        add_log('s', txt);

        game->in_game = 1;
        broadcastData("GSTART", "");

        // Send msg
        sprintf(txt, "<<<GSTART");
        send_msg_to_all_players(game, txt);

    } else if (strcmp(command[0], "GCREAT") == 0) { /* <================================ CREATE NEW GAME */
        // Create Game
        int game_id = create_game(player);
        sprintf(txt, "%d", game_id);
        broadcastData("GLIST", get_games_list());

    } else if (strcmp(command[0], "GCON") == 0) { /* <=================================== CONNECT TO GAME */
        // Check message
        if  (command[2] == NULL && command[3] == NULL) {
            invalid_command(command[0]);
            free(txt);
            return;
        }

        long int tmp = atol(command[2]);
        games_t *g = find_game(tmp);
        if (g)
            connect_to_game(player, g);

    } else if (strcmp(command[0], "GDISCON") == 0) {
        end_game(player, game);

    } else if (strcmp(command[0], "GURES") == 0) { /* <=================================== USER RESULTS */
        // Check message
        if  (command[2] == NULL && command[3] == NULL) {
            invalid_command(command[0]);
            free(txt);
            return;
        }

        strcpy(player->score, command[2]);
        sendData(player->socket, "GURES", get_users_score(game));

    } else if (strcmp(command[0], "GRES") == 0) { /* <=================================== USER RESULTS */
        // Check message
        if  (command[2] == NULL && command[3] == NULL) {
            invalid_command(command[0]);
            free(txt);
            return;
        }

        // Save all words
        strcpy(player->last_result, command[2]);
        sprintf(txt, "<<<GRES%s", get_users_results(game));
        send_msg_to_all_players(game, txt);

    } else if (strcmp(command[0], "GLETTER") == 0) { /* <=================================== USER RESULTS */
        sprintf(txt, "<<<GLETTER!%c", get_letter());
        send_msg_to_all_players(game, txt);

    } else if (strcmp(command[0], "GLIST") == 0) { /* <=================================== USER RESULTS */
        broadcastData("GLIST", get_games_list());
    } else { /* <======================================================================== INVALID COMMAND */
        invalid_command(command[0]);
    }

    free(txt);
}

/*
 * Receiving a message from the Client socket
 */
void *receiving(void *arg)
{
    players_t *player = (players_t*) arg;
    int client_sock = player -> socket;
    int id = player -> ID;
    char *tokens[15];
    int prijato;
    char cbuf[1024];
    char *txt = (char*)malloc(1024);

    while((prijato = recv(client_sock, cbuf, 1024 * sizeof(char), 0)) != 0)
    {
        if(prijato == -1)
        {
            if (player->game) {
                add_log('s', "recv() - ERR");
                sendData(player->socket, "SERROR", "");
                end_game(player, player->game);
            }
        }
        else
        {
            add_log('s', cbuf);
            received_bytes += prijato;
            received_msg++;

            check_valid_message(tokens, cbuf);

            if (tokens[0] != NULL) {
                parse_command(tokens);
                memset(cbuf, 0, 1024*sizeof(char));
            } else {
                invalid_command(cbuf);
            }

        }
    }

    // Delete player if exist
    player = find_player(id);
    if(player)
    {
        if(player->game)
            end_game(player, player -> game);

        delete_player_from_list(player);
    }
    free(txt);
    return 0;
}

/*
 * Connect new user and create user-receiving-thread
 */
void *request(void *arg)
{
    int client_sock = *(int *) arg;
    char command[64];
    char *tokens[15];
    char *user_name;
    char *txt = malloc(sizeof(char)*100);
    char *players_list;
    int flag = 1;

    memset(command,0, strlen(command));

    received_bytes += recv(client_sock, command, 64 * sizeof(char), 0);
    received_msg++;

    setsockopt (client_sock, SOL_SOCKET, SO_RCVTIMEO, &flag, sizeof(flag));

    // Check message
    check_valid_message(tokens, command);

    if (tokens[0] != NULL) {
        add_log('s', command);
        // Create player
        if(tokens[0] != NULL && strcmp(tokens[0], "CON") == 0)
        {
            user_name = tokens[1];

            if(user_name == NULL)
            {
                user_name = malloc(sizeof(char) * 10);
                sprintf(user_name, "Player");
            }

            players_t *new_player = create_player(client_sock, user_name);
            add_player_to_list(new_player);

            players_list = get_players_list();

            //Create user
            sprintf(txt, "%s:%d%s", new_player->name, new_player->ID, players_list);
            broadcastData("USERS", txt);

            // Logs
            sprintf(txt, "New user connected: %s : %d", new_player->name, new_player->ID);
            add_log('s', txt);
            free(players_list);
            free(txt);

            // Create new thread
            thread_id = 0;
            pthread_create(&thread_id, NULL, receiving, (void *) new_player);

        }
        else
        {
            invalid_command(command);
        }

    }
    else {
        add_log('s', "Socket shutdown. Further sends and receives are disallowed.");
        shutdown(client_sock, SHUT_WR);
        close(client_sock);
        invalid_command(command);
    }

    free(arg);

    return NULL;
}

/*
 * Create server socket, assigns the address, accept incoming connection
 * requests
 */
void *create_tcp_connection(void *arg)
{
    int server_sock;
    int client_sock;
    int return_value;
    int *th_socket;
    int port = *(int *)arg;

    struct sockaddr_in local_addr;
    struct sockaddr_in remote_addr;

    socklen_t remote_addr_len;

    server_sock = socket(AF_INET, SOCK_STREAM, 0);

    int flag = 1;
    setsockopt(server_sock, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(int));

    if (server_sock <= 0)
        return NULL;

    memset(&local_addr, 0, sizeof(struct sockaddr_in));

    local_addr.sin_family = AF_INET;
    local_addr.sin_port = htons(port);
    local_addr.sin_addr.s_addr = INADDR_ANY;

    // BIND
    return_value = bind(server_sock, (struct sockaddr *)&local_addr, sizeof(struct sockaddr_in));

    if (return_value == 0)
        add_log('s', "Bind - OK");
    else
    {
        add_log('s', "Bind - ERR");
        exit(1);
    }

    // LISTEN
    server_start_listen(server_sock, 5);

    while(1)
    {
        client_sock = accept(server_sock,(struct sockaddr *)&remote_addr, &remote_addr_len);

        if (client_sock > 0 )
        {
            th_socket = malloc(sizeof(int));
            *th_socket = client_sock;

            thread_id = 0;
            pthread_create(&thread_id, NULL,(void *)&request, (void *)th_socket);
        }
        else
        {
            add_log('s', "Socket error");
            exit(1);
        }
    }
}

/*
 * Accept incoming connection requests
 */
int server_start_listen (int server_socket, int backlog) {
    int return_value;

    return_value = listen(server_socket, backlog);
    if (return_value == 0){
        add_log('s', "Listen - OK");
    } else {
        add_log('s', "Listen - ERR");
        exit(1);
    }
    return return_value;
}

/*
 * Connect user to game
 */
void connect_to_game (players_t *player, games_t *game) {
    char *log = malloc(200 * sizeof(char));

    if(!player || !game)
        return;


    if(game->players_count >= 4)
        return;

    int i;
    for(i = 0; i < MAX_PLAYERS_COUNT; i++)
    {
        if((game->players)[i] == NULL)
        {
            (game->players)[i] = player;
            game->players_count++;
            player->game = game;
            break;
        }
    }
    sprintf(log, "User #%d connected to game #%d", player->ID, game->ID);
    add_log('s', log);

    // Send info to player
    sprintf(log, "<<<GUSERLIST!%s", get_game_players_list(player->game));
    send_msg_to_all_players(player->game, log);
    free(log);
}

/*
 * Disconnect user from game
 */
void end_game(players_t *player, games_t *game)
{
    games_t *tmp = game;

    if(!game || !player)
        return;

    int i;
    for(i = 0; i < MAX_PLAYERS_COUNT; i++)
    {
        if((game->players)[i] == player)
        {
            (game->players)[i] = NULL;
            (game->players_count)--;
            player->game = NULL;
            break;
        }

    }

    // Add log
    char *log = malloc(sizeof(char) * 200);
    sprintf(log, "Player #%d disconnected from game #%d",player->ID, game->ID);
    add_log('s', log);

    if(game->players_count == 0)
    {
        delete_game(game);
    }

    sprintf(log, "<<<GUSERLIST!%s", get_game_players_list(tmp));
    send_msg_to_all_players(tmp, log);

    free(log);
}

/*
 * Create random integer
 */
int getID () {
    int id;

    do {
        id = rand();
    }
    while(find_ID(id) != 0);


    return id;
}

/*
 * Check if random ID already exist
 */
int find_ID(int id)
{
    if(!id)
        return -1;

    // Players list
    pthread_mutex_lock(&m_players);
    if(players != NULL)
    {
        players_t *ptr = players;

        while(ptr != NULL)
        {
            if(ptr->ID == id)
            {
                pthread_mutex_unlock(&m_players);
                return 1;
            }

            ptr = ptr->next;
        }
    }
    pthread_mutex_unlock(&m_players);

    // Games list
    pthread_mutex_lock(&m_games);
    if(games != NULL)
    {
        games_t *ptr = games;

        while(ptr != NULL)
        {
            if(ptr->ID == id)
            {
                pthread_mutex_unlock(&m_games);
                return 1;
            }
            ptr = ptr->next;
        }
    }
    pthread_mutex_unlock(&m_games);

    return 0;
}

/*
 * Send message to all users
 */
void broadcastData (char *cmd, char *txt) {
    char *command = (char*)malloc(1024);

    strcpy(command, "<<<");
    strcat(command, cmd);
    strcat(command, "!");
    strcat(command, txt);
    strcat(command, "\0");

    pthread_mutex_lock(&m_players);
    if(players != NULL)
    {
        players_t *ptr = players;

        do
        {
            send_msg_bytes += send(ptr->socket , command , strlen(command) , 0);
            send_msg_count++;
            ptr = ptr->next;
        }while(ptr != NULL);

    }
    pthread_mutex_unlock(&m_players);

    add_log('c', command);
    free(command);
}

/*
 * Send message to one socket
 */
void sendData (int socket, char *cmd, char *txt) {
    char *command = (char*)malloc(1024);

    strcpy(command, "<<<");
    strcat(command, cmd);
    strcat(command, "!");
    strcat(command, txt);
    strcat(command, "\0");

    send_msg_bytes += send(socket , command , strlen(command) , 0 );
    send_msg_count++;

    add_log('c', command);
    free(command);
}

/*
 * Error message format
 */
void invalid_command (char *command) {
    char *txt = (char*)malloc(1024);
    sprintf(txt, "Invalid command: %s", command);
    add_log('s', txt);
    error_msg++;
}

/*
 * Convert integer to string
 */
char* integer_to_string(int x)
{
    char* buffer = malloc(30);
    if (buffer)
    {
        sprintf(buffer, "%d", x);
    }
    return buffer; 
}

/*
 * Print server information
 */
void get_server_info()
{
    time(&now);
    long sec = difftime(now, start);
    int min = sec / 60;
    int h = min / 60;

    printf("+================= [SERVER STATISTIC] ===================+\n");
    printf("\n");
    printf("\t * Received messages count: [%ld]\n", received_msg);
    printf("\t * Received bytes count: [%ld]\n", received_bytes);
    printf("\t * Send messages count: [%ld]\n", send_msg_count);
    printf("\t * Send bytes count: [%ld]\n", send_msg_bytes);
    printf("\t * Error messages count: [%ld]\n", error_msg);
    printf("\t * Server run [%d] hours [%d] minutes [%ld] sec.\n", h, min, sec%60);
    printf("\t * Server start: %s\n", asctime(localtime(&now)));
    printf("+========================================================+\n");
}

/*
 * Print using
 */
void using()
{
    printf("+======================= [USING] ========================+\n");
    printf("|    * ./server [port]                                   |\n");
    printf("+========================================================+\n");
}

/*
 * Start server information
 */
void server_start(char *port)
{
    printf("+==================== [START INFO] ======================+\n");
    printf("|    * Server will listen on port: [%s]               |\n", port);
    printf("|    * Server start: %s", get_current_time());
    printf("+========================================================+\n");
}
