#ifndef SERVER_SERVER_H
#define SERVER_SERVER_H

#include "structures.h"

// Server start
int server_start_listen (int server_socket, int backlog);
void *create_tcp_connection(void *arg);
void *request(void *arg);
void *receiving(void *arg);
void parse_command(char *command[15]);
void check_valid_message (char* tokens[15], char command[64]);

// Game connection functions
void connect_to_game (players_t *player, games_t *game);
void end_game(players_t *player, games_t *game);

// Send functions
void broadcastData (char *cmd, char *txt);
void sendData (int socket, char *cmd, char *txt);

// Helpers
int find_ID(int id);
int getID ();
void invalid_command (char *command);
char* integer_to_string(int x);
void using();
void server_start(char *port);

// Statistic
void get_server_info();

#endif //SERVER_SERVER_H
