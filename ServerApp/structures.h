#ifndef SERVERAPP_STRUCTURES_H
#define SERVERAPP_STRUCTURES_H

typedef struct PLAYER_LIST {
    int socket;
    char *name;
    int ID;
    char *score;
    char *last_result;
    struct PLAYER_LIST *next;
    struct GAME_LIST *game;
} players_t;

typedef struct GAME_LIST {
    int ID;
    char *name;
    players_t *players[10];
    int in_game;
    int players_count;
    struct GAME_LIST* next;
} games_t;



#endif //SERVERAPP_STRUCTURES_H
